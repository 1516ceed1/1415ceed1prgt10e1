/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.modelo;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public interface IModelo {

   // Alumno
   public void create(Alumno a); // Crea uno nuevo

   public void update(Alumno a); // Actuzaliza uno

   public void delete(Alumno a);  // Borrar uno

   public ArrayList<Alumno> reada(); // Obtiene todos

   // Grupo
   public void create(Grupo g); // Crea uno nuevo

   public void update(Grupo g); // Actuzaliza uno

   public void delete(Grupo g);  // Borrar uno

   public ArrayList<Grupo> readg(); // Obtiene todos

   public void instalar();  // Instalar Ficheros/ BDR / BDOO

   public void desinstalar();  // Desistalar  Ficheros/ BDR / BDOO

}
