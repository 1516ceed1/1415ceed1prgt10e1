/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.modelo;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed1prgt10e1.controlador.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author paco
 */
public class ModeloHibernate implements IModelo {

   private static String bd = "1516ceedprg";
   private static String user = "alumno";
   private static String pass = "alumno";
   private Connection con = null;
   private Statement st = null;
   private ResultSet rs = null;
   private int ida = 1;
   private int idg = 1;

   @Override
   public void create(Alumno alumno) {
      try {

         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();

         session.save(alumno);
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
   }

   @Override
   public void update(Alumno alumno) {
      try {

         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();

         session.update(alumno);
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
   }

   @Override
   public void delete(Alumno alumno) {
      try {

         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();

         session.delete(alumno);
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
   }

   @Override
   public ArrayList<Alumno> reada() {
      ArrayList alumnos = new ArrayList<Alumno>();
      try {
         String hql = "from Alumno";
         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
         Query q = session.createQuery(hql);
         alumnos = (ArrayList) q.list();
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
      return alumnos;
   }

   @Override
   public void create(Grupo grupo) {
      try {

         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
         session.save(grupo);
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
   }

   @Override
   public void update(Grupo grupo) {
      try {

         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();

         session.update(grupo);
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
   }

   @Override
   public void delete(Grupo grupo) {
      try {

         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();

         session.delete(grupo);
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
   }

   @Override
   public ArrayList<Grupo> readg() {
      ArrayList grupos = new ArrayList<Grupo>();
      try {
         String hql = "from Grupo";
         Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
         Query q = session.createQuery(hql);
         grupos = (ArrayList) q.list();
         session.getTransaction().commit();

      } catch (HibernateException he) {
         he.printStackTrace();
      }
      return grupos;
   }

   @Override
   public void instalar() {

      String error = createbd();
      if (error != null) {
         org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, error);
         return;
      }

      error = creartablas();
      if (error != null) {
         org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, error);
         return;
      }

      org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, "Instalación correcta");
   }

   public void desinstalar() {
      Connection con = null;
      Statement st = null;
      String error = null;

      try {

         String driver = "com.mysql.jdbc.Driver";
         Class.forName(driver).newInstance();
         String jdbcUrl = "jdbc:mysql://localhost:3306/mysql";
         con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);

         st = con.createStatement();
         String sql = "DROP DATABASE IF EXISTS " + bd + ";";
         System.out.println(sql);
         st.executeUpdate(sql);
      } catch (Exception e) {
         error = e.getMessage();
      } finally {
         if (st != null) {
            try {
               st.close();
            } catch (SQLException e) {
               error = e.getMessage();
            } // nothing we can do
         }
         if (con != null) {
            try {
               con.close();
            } catch (SQLException se) {
               error = se.getMessage();
            } // nothing we can do
         }
      }

   }

   public String creartablas() {
      String error = null;
      try {

         String sql;

         conectar();
         st = con.createStatement();

         sql = "drop table if exists alumno;";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "drop table if exists grupo;";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "CREATE TABLE IF NOT EXISTS `grupo` (\n"
                 + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
                 + "`nombre` varchar(35) NOT NULL,\n"
                 + "PRIMARY KEY (`id`)\n"
                 + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "insert into grupo(id, nombre) values(1,'Grupo 1');\n";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "insert into grupo(id, nombre) values(2,'Grupo 2');\n";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "CREATE TABLE IF NOT EXISTS `alumno` (\n"
                 + "  `id` int(5) NOT NULL AUTO_INCREMENT,\n"
                 + "  `nombre` varchar(25) ,\n"
                 + "  `edad` int(11) ,\n"
                 + "  `email` varchar(25),\n"
                 + "  `fecha` date,\n"
                 + "  `idg` int(5),\n"
                 + "  PRIMARY KEY (`id`),\n"
                 + "  FOREIGN KEY (`idg`) REFERENCES grupo(id)\n"
                 + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "insert into alumno(id, nombre,edad,email,fecha,idg) values(1,'Juan',25,'juan@gmail.com','2016-2-25',1);\n";
         System.out.println(sql);
         st.executeUpdate(sql);

         sql = "insert into alumno(id, nombre,edad,email,fecha,idg) values(2,'Juan',25,'juan@gmail.com','2016-2-25',2);\n";
         System.out.println(sql);
         st.executeUpdate(sql);

         st.close();
      } catch (SQLException ex) {
         error = ex.getMessage();
         Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
      }
      desconectar();
      return error;
   }

   public String createbd() {

      ida = 1;
      idg = 1;

      Connection con = null;
      Statement st = null;
      String error = null;

      try {

         String driver = "com.mysql.jdbc.Driver";
         Class.forName(driver).newInstance();
         String jdbcUrl = "jdbc:mysql://localhost:3306/mysql";
         con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);

         st = con.createStatement();
         String sql = "CREATE DATABASE IF NOT EXISTS " + bd + ";";
         System.out.println(sql);
         st.executeUpdate(sql);
      } catch (Exception e) {
         error = e.getMessage();
      } finally {
         if (st != null) {
            try {
               st.close();
            } catch (SQLException e) {
               error = e.getMessage();
            } // nothing we can do
         }
         if (con != null) {
            try {
               con.close();
            } catch (SQLException se) {
               error = se.getMessage();
            } // nothing we can do
         }
      }
      return error;
   }

   public void desconectar() {
      try {
         System.out.println("BDR Mysql Connexión cerrada");
         con.close();
      } catch (SQLException se) {
         se.printStackTrace();
      }
   }

   public void conectar() {

      try {
         //Registrando el Driver
         String driver = "com.mysql.jdbc.Driver";
         Class.forName(driver).newInstance();
         //System.out.println("Driver Registrado correctamente");
         //Abrir la conexion con la Base de Datos
         //System.out.println("Conectando con la Base de datos...");
         String jdbcUrl = "jdbc:mysql://localhost:3306/" + bd;
         con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);
         System.out.println("Conexion establecida con la Base de datos: " + bd);

      } catch (SQLException se) {
         //Errores de JDBC
         se.printStackTrace();
      } catch (Exception e) {
         //Errores debidos al Class.forName
         e.printStackTrace();
      }//end try

   }

}
