package org.ceedcv.ceed1prgt10e1.modelo;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed1prgt10e1.vista.Funciones;

/**
 *
 * @author paco
 */
public class ModeloMysql implements IModelo {

    private static String bd = "1516ceedprg";
    private static String user = "alumno";
    private static String pass = "alumno";
    private Connection con = null;
    private Statement st = null;
    private ResultSet rs = null;
    private int ida = 1;
    private int idg = 1;

    public ModeloMysql() {

    }

    public void desconectar() {
        try {
            System.out.println("BDR Mysql Connexión cerrada");
            con.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public void conectar() {

        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            //System.out.println("Driver Registrado correctamente");
            //Abrir la conexion con la Base de Datos
            //System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/" + bd;
            con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);
            System.out.println("Conexion establecida con la Base de datos: " + bd);

        } catch (SQLException se) {
            Funciones.mostarTexto(null, "No existe la BD. Instalarla");
            //Errores de JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Errores debidos al Class.forName
            Funciones.mostarTexto(null, "Instalar el JDBC");
            e.printStackTrace();
        }//end try

    }

    public String creartablas() {
        String error = null;
        try {

            String sql;

            conectar();
            st = con.createStatement();

            sql = "drop table if exists alumno;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "drop table if exists grupo;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS `grupo` (\n"
              + "`idg` int(5) NOT NULL AUTO_INCREMENT,\n"
              + "`nombreg` varchar(35) NOT NULL,\n"
              + "PRIMARY KEY (`idg`)\n"
              + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "insert into grupo(idg, nombreg) values(1,'Grupo 1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "insert into grupo(idg, nombreg) values(2,'Grupo 2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS `alumno` (\n"
              + "  `ida` int(5) NOT NULL AUTO_INCREMENT,\n"
              + "  `nombrea` varchar(25) ,\n"
              + "  `edad` int(11) ,\n"
              + "  `email` varchar(25),\n"
              + "  `fecha` date,\n"
              + "  `idg` int(5),\n"
              + "  PRIMARY KEY (`ida`),\n"
              + "  FOREIGN KEY (`idg`) REFERENCES grupo(idg)\n"
              + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "insert into alumno(ida, nombrea,edad,email,fecha,idg) values(1,'Juan',25,'juan@gmail.com','2016-2-25',1);\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "insert into alumno(ida, nombrea,edad,email,fecha,idg) values(2,'Juan',25,'juan@gmail.com','2016-2-25',2);\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
        }
        desconectar();
        return error;
    }

    public String createbd() {

        ida = 1;
        idg = 1;

        Connection con = null;
        Statement st = null;
        String error = null;

        try {

            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            String jdbcUrl = "jdbc:mysql://localhost:3306/mysql";
            con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);

            st = con.createStatement();
            String sql = "CREATE DATABASE IF NOT EXISTS " + bd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
        } catch (Exception e) {
            error = e.getMessage();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    error = e.getMessage();
                } // nothing we can do
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException se) {
                    error = se.getMessage();
                } // nothing we can do
            }
        }
        return error;
    }

    @Override
    public void create(Alumno alumno) {
        int resultado;
        int id_;
        String sql;

        try {
            conectar();
            st = con.createStatement();

            String nombre = alumno.getNombre();
            int edad = alumno.getEdad();
            String email = alumno.getEmail();
            Date fecha = alumno.getFecha();
            Funciones f = new Funciones();
            String sfecha = f.ConvertirDatetoSTring(fecha);
            Integer idg = alumno.getGrupo().getId();

            sql = "insert into alumno( nombrea, edad , email , fecha , idg) "
              + "values('" + nombre + "','" + edad + "','" + email + "','"
              + sfecha + "','" + idg + "')";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);

            sql = "select max(ida) as maxid from alumno;";
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                ida = rs.getInt("maxid");
            }
            alumno.setId(ida);
            desconectar();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void update(Alumno alumno) {
        int resultado;
        String sql;

        try {
            conectar();
            st = con.createStatement();
            Funciones f = new Funciones();
            String sFecha = f.ConvertirDatetoSTring(alumno.getFecha());
            sql = "update alumno  set nombrea='" + alumno.getNombre()
              + "', edad=" + alumno.getEdad()
              + ",  email='" + alumno.getEmail()
              + "', fecha='" + sFecha
              + "', idg=" + alumno.getGrupo().getId()
              + " where ida="
              + alumno.getId()
              + ";";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);

            desconectar();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void delete(Alumno alumno) {
        int resultado;
        String sql;
        try {
            conectar();
            st = con.createStatement();

            sql = "delete from alumno where ida="
              + alumno.getId()
              + ";";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);
            desconectar();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void create(Grupo grupo) {
        int resultado;
        int id_;
        String sql;

        try {
            conectar();
            st = con.createStatement();

            String nombre = grupo.getNombre();

            sql = "insert into grupo(nombreg) "
              + "values('" + nombre + "');";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);

            sql = "select max(idg) as maxid from grupo;";
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                idg = rs.getInt("maxid");
            }
            grupo.setId(idg);
            desconectar();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void update(Grupo grupo) {
        int resultado;
        String sql;

        try {
            conectar();
            st = con.createStatement();

            sql = "update grupo"
              + " set nombreg ='" + grupo.getNombre()
              + "' where idg="
              + grupo.getId()
              + ";";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);
            desconectar();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void delete(Grupo grupo) {
        int resultado;
        String sql;
        try {
            conectar();
            st = con.createStatement();

            sql = "delete from grupo where idg="
              + grupo.getId()
              + ";";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);
            desconectar();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public ArrayList<Grupo> readg() {
        ArrayList<Grupo> grupos = new ArrayList();

        try {

            conectar();
            st = con.createStatement();
            String sql = "select idg,nombreg from grupo order by idg;";
            System.out.println(sql);
            rs = st.executeQuery(sql);

            while (rs.next()) {
                Grupo grupo = new Grupo();
                Integer idg;
                String nombreg;
                idg = rs.getInt("idg");
                nombreg = rs.getString("nombreg");
                grupo.setId(idg);
                grupo.setNombre(nombreg);
                grupos.add(grupo);
            }

        } catch (SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Errores de Class.forNameCliente
            e.printStackTrace();
        }
        desconectar();
        return grupos;
    }

    @Override
    public ArrayList<Alumno> reada() {

        ArrayList<Alumno> alumnos = new ArrayList();

        try {

            conectar();
            st = con.createStatement();

            //Ejecutamos la SELECT sobre la tabla alumno
            String sql = "select ida,nombrea,edad,email,fecha,g.idg,nombreg "
              + "from alumno as a, grupo as g where a.idg=g.idg order by ida;";
            System.out.println(sql);

            rs = st.executeQuery(sql);

            while (rs.next()) {

                Alumno alumno = new Alumno();
                Grupo grupo = new Grupo();

                Integer ida;
                String nombrea;
                int edad;
                String email;
                Date fecha;
                Integer idg;
                String nombreg;

                ida = rs.getInt("ida");
                nombrea = rs.getString("nombrea");
                edad = rs.getInt("edad");
                email = rs.getString("email");
                fecha = rs.getDate("fecha");
                idg = rs.getInt("idg");
                nombreg = rs.getString("nombreg");

                alumno.setId(ida);
                alumno.setNombre(nombrea);
                alumno.setEdad(edad);
                alumno.setEmail(email);
                alumno.setFecha(fecha);

                grupo.setId(idg);
                grupo.setNombre(nombreg);

                alumno.setGrupo(grupo);
                alumnos.add(alumno);

            }

        } catch (SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Errores de Class.forNameCliente
            e.printStackTrace();
        }
        desconectar();
        return alumnos;
    }

    @Override
    public void instalar() {

        String error = createbd();
        if (error != null) {
            org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, error);
            return;
        }

        error = creartablas();
        if (error != null) {
            org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, error);
            return;
        }

        org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, "Instalación correcta");
    }

    public void desinstalar() {
        Connection con = null;
        Statement st = null;
        String error = null;

        try {

            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            String jdbcUrl = "jdbc:mysql://localhost:3306/mysql";
            con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);

            st = con.createStatement();
            String sql = "DROP DATABASE IF EXISTS " + bd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
        } catch (Exception e) {
            error = e.getMessage();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    error = e.getMessage();
                } // nothing we can do
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException se) {
                    error = se.getMessage();
                } // nothing we can do
            }
        }

    }

}
