package org.ceedcv.ceed1prgt10e1.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.ceedcv.ceed1prgt10e1.vista.Funciones;

/**
 *
 * @author paco
 */
public class ModeloFichero implements IModelo {

    private File falumnos;
    private File fgrupos;
    private int ida = 0;
    private int idg = 0;
    private static String grupos = "grupos.cvs";
    private static String alumnos = "alumnos.cvs";

    private int calculaida() {
        int idmax = 0;

        if (falumnos.exists()) {

            try {

                FileReader fr = new FileReader(falumnos);
                BufferedReader br = new BufferedReader(fr);
                Alumno alumno;
                String linea;
                int id_;

                linea = br.readLine();
                while (linea != null) {
                    alumno = extraeAlumno(linea);
                    id_ = alumno.getId();
                    if (id_ > idmax) {
                        idmax = id_;
                    }
                    linea = br.readLine();
                }

                fr.close();

            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

        }
        return idmax;

    }

    private int calculaidg() {
        int idmax = 0;

        if (fgrupos.exists()) {

            try {

                FileReader fr = new FileReader(fgrupos);
                BufferedReader br = new BufferedReader(fr);
                Grupo grupo;
                String linea;
                int id_;

                linea = br.readLine();
                while (linea != null) {
                    grupo = extraeGrupo(linea);
                    id_ = grupo.getId();
                    if (id_ > idmax) {
                        idmax = id_;
                    }
                    linea = br.readLine();
                }

                fr.close();

            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

        }
        return idmax;

    }

    public ModeloFichero() throws IOException {
        falumnos = new File(alumnos);
        fgrupos = new File(grupos);

        if (!falumnos.exists() || !falumnos.exists()) {
            Funciones.mostarTexto(null, "No existe ficheros. Instalarlos");
        }

        ida = calculaida();
        idg = calculaidg();
    }

    @Override
    public void create(Alumno alumno) {
        ida++;
        alumno.setId(ida);
        FileWriter fw = null;
        try {
            fw = new FileWriter(falumnos, true);
            grabarAlumno(alumno, fw);
            fw.close();
        } catch (IOException ex) {
        }

    }

    @Override
    public void create(Grupo grupo) {
        idg++;
        grupo.setId(idg);
        FileWriter fw = null;
        try {
            fw = new FileWriter(fgrupos, true);
            grabarGrupo(grupo, fw);
            fw.close();
        } catch (IOException ex) {
        }

    }

    private Alumno extraeAlumno(String linea) {

        Alumno alumno;
        StringTokenizer str = new StringTokenizer(linea, ";");

        String id = str.nextToken();
        String nombre = str.nextToken();
        String edad_ = str.nextToken();
        int edad = Integer.parseInt(edad_);
        String email = str.nextToken();
        String fecha_ = str.nextToken();

        Funciones f = new Funciones();
        Date fecha = f.ConvertirStringtoDate(fecha_);

        String idgrupo = str.nextToken();
        int id_ = Integer.parseInt(idgrupo);

        alumno = new Alumno();
        alumno.setId(id_);
        alumno.setNombre(nombre);
        alumno.setEdad(edad);
        alumno.setFecha(fecha);
        alumno.setEmail(email);

        Grupo grupo = new Grupo();
        grupo.setId(id_);

        alumno.setGrupo(grupo);

        return alumno;
    }

    private Grupo extraeGrupo(String linea) {

        Grupo grupo;
        StringTokenizer str = new StringTokenizer(linea, ";");

        String id = str.nextToken();
        String nombre = str.nextToken();

        grupo = new Grupo();
        grupo.setId(Integer.parseInt(id));
        grupo.setNombre(nombre);
        return grupo;
    }

    @Override
    public ArrayList reada() {

        ArrayList alumnos = new ArrayList();

        if (!falumnos.exists()) {
            return alumnos;
        }

        Grupo grupo;
        try {
            FileReader fr = new FileReader(falumnos);
            BufferedReader br = new BufferedReader(fr);
            Alumno alumno;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                alumno = extraeAlumno(linea);
                grupo = getGrupo(alumno.getGrupo().getId());
                alumno.setGrupo(grupo);
                alumnos.add(alumno);
                linea = br.readLine();
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        return alumnos;
    }

    @Override
    public void update(Alumno alumno) {

        try {
            File temp = new File("temp.txt");
            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(falumnos);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (a.getId() == alumno.getId()) {
                    grabarAlumno(alumno, fw);
                } else {
                    grabarAlumno(a, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            falumnos.delete();
            temp.renameTo(falumnos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    @Override
    public void delete(Alumno alumno) {

        try {

            File temp = new File("temp.txt");

            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(falumnos);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (a.getId() != alumno.getId()) {
                    grabarAlumno(a, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            falumnos.delete();
            temp.renameTo(falumnos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    private void grabarAlumno(Alumno alumno, FileWriter fw) throws IOException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        fw.write(alumno.getId() + "");
        fw.write(";");
        fw.write(alumno.getNombre());
        fw.write(";");
        fw.write(alumno.getEdad() + "");
        fw.write(";");
        fw.write(alumno.getEmail());
        fw.write(";");
        fw.write(sdf.format(alumno.getFecha()));
        fw.write(";");
        fw.write(alumno.getGrupo().getId() + "");
        fw.write(";");
        fw.write("\r\n");
    }

    private void grabarGrupo(Grupo grupo, FileWriter fw) throws IOException {
        fw.write(grupo.getId() + "");
        fw.write(";");
        fw.write(grupo.getNombre());
        fw.write(";");
        fw.write("\r\n");
    }

    @Override
    public void update(Grupo grupo) {
        try {
            File temp = new File("temp.txt");
            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fgrupos);

            BufferedReader br = new BufferedReader(fr);
            Grupo g;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                g = extraeGrupo(linea);
                if (g.getId() == grupo.getId()) {
                    grabarGrupo(grupo, fw);
                } else {
                    grabarGrupo(g, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fgrupos.delete();
            temp.renameTo(fgrupos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    @Override
    public void delete(Grupo grupo) {
        try {

            File temp = new File("temp.txt");

            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fgrupos);

            BufferedReader br = new BufferedReader(fr);
            Grupo g;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                g = extraeGrupo(linea);
                if (g.getId() != grupo.getId()) {
                    grabarGrupo(g, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fgrupos.delete();
            temp.renameTo(fgrupos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    @Override
    public ArrayList<Grupo> readg() {

        ArrayList grupos = new ArrayList();

        if (!fgrupos.exists()) {

            return grupos;
        }

        try {
            FileReader fr = new FileReader(fgrupos);
            BufferedReader br = new BufferedReader(fr);
            Grupo grupo;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                grupo = extraeGrupo(linea);
                grupos.add(grupo);
                linea = br.readLine();
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        return grupos;
    }

    private Grupo getGrupo(Integer id) {

        Grupo encontrado = null;
        ArrayList grupos = readg();
        Iterator it = grupos.iterator();
        while (it.hasNext()) {
            Grupo grupo = (Grupo) it.next();
            if (id == grupo.getId()) {
                encontrado = grupo;
            }
        }
        return encontrado;

    }

    @Override
    public void instalar() {

        ida = 0;
        idg = 0;

        Funciones f = new Funciones();

        Alumno alumno = new Alumno();
        Grupo grupo = new Grupo();

        grupo.setId(1);
        grupo.setNombre("Grupo 1");

        alumno.setId(1);
        alumno.setNombre("Paco Aldarias");
        alumno.setEdad(25);

        alumno.setFecha(f.ConvertirStringtoDate("22-03-2016"));
        alumno.setEmail("paco.aldarias@ceedcv.es");

        alumno.setGrupo(grupo);
        create(alumno);
        create(grupo);

        // alumno 2
        Alumno alumno2 = new Alumno();
        Grupo grupo2 = new Grupo();
        grupo2.setId(2);
        grupo2.setNombre("Grupo 2");

        alumno2.setId(1);
        alumno2.setNombre("Pepe Garcia");
        alumno2.setEdad(25);

        alumno2.setFecha(f.ConvertirStringtoDate("2-05-2016"));
        alumno2.setEmail("pepe.garcia@ceedcv.es");

        alumno2.setGrupo(grupo2);

        create(alumno2);
        create(grupo2);

    }

    @Override
    public void desinstalar() {
        fgrupos.delete();
        falumnos.delete();
    }

}
