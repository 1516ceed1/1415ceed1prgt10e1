package org.ceedcv.ceed1prgt10e1.modelo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.ceedcv.ceed1prgt10e1.vista.Funciones;

/**
 *
 * @author paco
 */
public class ModeloDb4o implements IModelo {

    private int ida = 1;
    private int idg = 1;
    private static String fichero = "bdoo.db4o";
    private File ficherobdoo = new File(fichero);
    private ObjectContainer bd;

    private int calculaida() {
        int idmax = 0;

        Alumno vacio = AlumnoVacio();
        Alumno a = null;
        int id;

        conectar();
        ObjectSet res = bd.queryByExample(vacio);
        while (res.hasNext()) {
            a = (Alumno) res.next();
            id = a.getId();
            if (id > idmax) {
                idmax = id;
            }
        }
        desconectar();
        return idmax + 1;

    }

    private int calculaidg() {
        int idmax = 0;

        Grupo vacio = new Grupo(0, null);
        Grupo g = null;
        int id;

        conectar();
        ObjectSet res = bd.queryByExample(vacio);
        while (res.hasNext()) {
            g = (Grupo) res.next();
            id = g.getId();
            if (id > idmax) {
                idmax = id;
            }
        }
        desconectar();
        return idmax + 1;
    }

    public ModeloDb4o() throws IOException {

        if (ficherobdoo.exists()) {
            ida = calculaida();
            idg = calculaidg();
        } else {
            Funciones.mostarTexto(null, "No existe BD. Debes instalarla");
        }
    }

    public void conectar() {
        System.out.println("BDOO Connexión establecida");
        bd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), fichero);
    }

    public void desconectar() {
        System.out.println("BDOO Connexión cerrada");
        bd.close();
    }

    @Override
    public void create(Alumno alumno) {

        conectar();
        alumno.setId(ida);
        System.out.println("Insertado Alumno " + alumno.toString());
        ida++;

        // El objeto grupo debe ser el existente en la bdoo
        Grupo grupo = grupoAlumno(alumno);
        if (grupo != null) {  // Si existe el grupo lo cogemos
            alumno.setGrupo(grupo);
        }

        bd.store(alumno);
        desconectar();
    }

    @Override
    public void update(Alumno alumno_) {

        Alumno alumno = null;
        Alumno buscado = new Alumno(alumno_.getId(), null, 0, null, null, null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            alumno = (Alumno) res.next();
            System.out.println("Update " + alumno.toString());
            alumno.setNombre(alumno_.getNombre());
            alumno.setEdad(alumno_.getEdad());
            alumno.setEmail(alumno_.getEmail());
            bd.store(alumno);
        }
        desconectar();

    }

    @Override
    public void delete(Alumno alumno_) {

        Alumno alumno = null;
        Alumno buscado = new Alumno(alumno_.getId(), null, 0, null, null, null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            alumno = (Alumno) res.next();
            System.out.println("Delete " + alumno.toString());
            bd.delete(alumno);
        }
        desconectar();

    }

    @Override
    public ArrayList<Alumno> reada() {
        Alumno alumno;
        ArrayList<Alumno> alumnos = new ArrayList();
        Alumno buscado = new Alumno(0, null, 0, null, null, null);
        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        while (res.hasNext()) {
            alumno = (Alumno) res.next();
            alumnos.add(alumno);
        }
        desconectar();
        return alumnos;
    }

    @Override
    public void create(Grupo grupo) {
        conectar();
        grupo.setId(idg);
        System.out.println("Insertado Grupo " + grupo.toString());
        idg++;
        bd.store(grupo);
        desconectar();
    }

    @Override
    public void update(Grupo grupo_) {
        Grupo grupo = null;
        Grupo buscado = new Grupo(grupo_.getId(), null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            grupo = (Grupo) res.next();
            System.out.println("Update " + grupo.toString());
            grupo.setNombre(grupo_.getNombre());
            bd.store(grupo);
        }
        desconectar();
    }

    @Override
    public void delete(Grupo grupo_) {
        Grupo grupo = null;
        Grupo buscado = new Grupo(grupo_.getId(), null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            grupo = (Grupo) res.next();
            System.out.println("Delete " + grupo.toString());
            bd.delete(grupo);
        }
        desconectar();
    }

    @Override
    public ArrayList<Grupo> readg() {
        Grupo grupo;
        ArrayList<Grupo> grupos = new ArrayList();
        Grupo buscado = new Grupo(0, null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        while (res.hasNext()) {
            grupo = (Grupo) res.next();
            grupos.add(grupo);
        }
        desconectar();
        return grupos;
    }

    @Override
    public void instalar() {

        Funciones f = new Funciones();

        Alumno alumno1 = new Alumno();
        Alumno alumno2 = new Alumno();
        Grupo grupo1 = new Grupo();
        Grupo grupo2 = new Grupo();

        grupo1.setId(1);
        grupo1.setNombre("Grupo 1");
        create(grupo1);

        grupo2.setId(2);
        grupo2.setNombre("Grupo 2");
        create(grupo2);

        alumno1.setId(1);
        alumno1.setNombre("Paco");
        alumno1.setEdad(25);
        alumno1.setFecha(f.ConvertirStringtoDate("22-03-2016"));
        alumno1.setEmail("paco.aldarias@ceedcv.es");

        alumno1.setGrupo(grupo1);
        create(alumno1);

        alumno2.setId(2);
        alumno2.setNombre("Juan");
        alumno2.setEdad(35);
        alumno2.setFecha(f.ConvertirStringtoDate("22-03-2015"));
        alumno2.setEmail("juan.perez@ceedcv.es");

        alumno2.setGrupo(grupo2);
        create(alumno2);

        f.mostarTexto(null, "Instalación correcta");
        ida = 3;
        idg = 3;

    }

    @Override
    public void desinstalar() {

        ida = 1;
        idg = 1;

        if (ficherobdoo.exists()) {
            ficherobdoo.delete();
            org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, "Desinstalación correcta");
        } else {
            org.ceedcv.ceed1prgt10e1.vista.Funciones.mostarTexto(null, "No existe fichero");
        }

    }

    private Grupo grupoAlumno(Alumno alumno) {
        Grupo grupo = null;
        Grupo buscado = new Grupo(alumno.getGrupo().getId(), null);

        ObjectSet res = bd.queryByExample(buscado);
        grupo = (Grupo) res.next();

        return grupo;
    }

    private Alumno AlumnoVacio() {

        Alumno alumno = new Alumno();

        alumno.setId(0);
        alumno.setNombre("");
        alumno.setEdad(0);
        alumno.setEmail("");
        alumno.setGrupo(null);

        return alumno;
    }

}
