/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.vista;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author paco
 */
@Entity
@Table(name = "alumno", catalog = "1516ceedprg", schema = "")
@NamedQueries({
    @NamedQuery(name = "Alumno.findAll", query = "SELECT a FROM Alumno a"),
    @NamedQuery(name = "Alumno.findByIda", query = "SELECT a FROM Alumno a WHERE a.ida = :ida"),
    @NamedQuery(name = "Alumno.findByNombrea", query = "SELECT a FROM Alumno a WHERE a.nombrea = :nombrea"),
    @NamedQuery(name = "Alumno.findByEdad", query = "SELECT a FROM Alumno a WHERE a.edad = :edad"),
    @NamedQuery(name = "Alumno.findByEmail", query = "SELECT a FROM Alumno a WHERE a.email = :email"),
    @NamedQuery(name = "Alumno.findByFecha", query = "SELECT a FROM Alumno a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "Alumno.findByIdg", query = "SELECT a FROM Alumno a WHERE a.idg = :idg")})
public class Alumno implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ida")
    private Integer ida;
    @Column(name = "nombrea")
    private String nombrea;
    @Column(name = "edad")
    private Integer edad;
    @Column(name = "email")
    private String email;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "idg")
    private Integer idg;

    public Alumno() {
    }

    public Alumno(Integer ida) {
        this.ida = ida;
    }

    public Integer getIda() {
        return ida;
    }

    public void setIda(Integer ida) {
        Integer oldIda = this.ida;
        this.ida = ida;
        changeSupport.firePropertyChange("ida", oldIda, ida);
    }

    public String getNombrea() {
        return nombrea;
    }

    public void setNombrea(String nombrea) {
        String oldNombrea = this.nombrea;
        this.nombrea = nombrea;
        changeSupport.firePropertyChange("nombrea", oldNombrea, nombrea);
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        Integer oldEdad = this.edad;
        this.edad = edad;
        changeSupport.firePropertyChange("edad", oldEdad, edad);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        String oldEmail = this.email;
        this.email = email;
        changeSupport.firePropertyChange("email", oldEmail, email);
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        Date oldFecha = this.fecha;
        this.fecha = fecha;
        changeSupport.firePropertyChange("fecha", oldFecha, fecha);
    }

    public Integer getIdg() {
        return idg;
    }

    public void setIdg(Integer idg) {
        Integer oldIdg = this.idg;
        this.idg = idg;
        changeSupport.firePropertyChange("idg", oldIdg, idg);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ida != null ? ida.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumno)) {
            return false;
        }
        Alumno other = (Alumno) object;
        if ((this.ida == null && other.ida != null) || (this.ida != null && !this.ida.equals(other.ida))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ceedcv.ceed1prgt10e1.vista.Alumno[ ida=" + ida + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
