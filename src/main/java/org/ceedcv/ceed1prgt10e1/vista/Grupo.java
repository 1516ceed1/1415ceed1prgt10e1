/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.vista;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author paco
 */
@Entity
@Table(name = "grupo", catalog = "1516ceedprg", schema = "")
@NamedQueries({
    @NamedQuery(name = "Grupo.findAll", query = "SELECT g FROM Grupo g"),
    @NamedQuery(name = "Grupo.findByIdg", query = "SELECT g FROM Grupo g WHERE g.idg = :idg"),
    @NamedQuery(name = "Grupo.findByNombreg", query = "SELECT g FROM Grupo g WHERE g.nombreg = :nombreg")})
public class Grupo implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idg")
    private Integer idg;
    @Basic(optional = false)
    @Column(name = "nombreg")
    private String nombreg;

    public Grupo() {
    }

    public Grupo(Integer idg) {
        this.idg = idg;
    }

    public Grupo(Integer idg, String nombreg) {
        this.idg = idg;
        this.nombreg = nombreg;
    }

    public Integer getIdg() {
        return idg;
    }

    public void setIdg(Integer idg) {
        Integer oldIdg = this.idg;
        this.idg = idg;
        changeSupport.firePropertyChange("idg", oldIdg, idg);
    }

    public String getNombreg() {
        return nombreg;
    }

    public void setNombreg(String nombreg) {
        String oldNombreg = this.nombreg;
        this.nombreg = nombreg;
        changeSupport.firePropertyChange("nombreg", oldNombreg, nombreg);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idg != null ? idg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if ((this.idg == null && other.idg != null) || (this.idg != null && !this.idg.equals(other.idg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ceedcv.ceed1prgt10e1.vista.Grupo[ idg=" + idg + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
