/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.vista;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 */
public class Funciones {

    public Date ConvertirStringtoDate(String strFecha) {
        Date fecha = null;
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        try {
            fecha = formatoDelTexto.parse(strFecha);
        } catch (ParseException ex) {
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fecha;
    }

    public String ConvertirDatetoSTring(Date fecha) {

        String sfecha = null;
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");

        if (fecha == null) {
            return "";
        }

        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd");
        sfecha = fechaHora.format(fecha);

        return sfecha;
    }

    public static void mostarTexto(JFrame frame, String s) {
        JOptionPane.showMessageDialog(frame, s);
    }

    public int aviso(String texto) {
        int i;
        i = JOptionPane.showConfirmDialog(null, texto, "Aviso", JOptionPane.WARNING_MESSAGE);
        return i;
    }

    public void cierraFrames(VistaPrincipal vistaPrincipal) {
        boolean existe = false;
        JInternalFrame[] frames = vistaPrincipal.getEscritorio().getAllFrames();
        JInternalFrame frame;
        int talla = frames.length;
        int i = 0;

        while (i < talla) {
            frames[i].dispose();
            i++;
        }

    }

}
