package org.ceedcv.ceed1prgt10e1.vista;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 */
public class Verificador extends InputVerifier {

   private String variable;

   public Verificador(String variable) {
      this.variable = variable;
   }

   public static boolean esValidoEmail(String s) {
      Pattern p = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
      Matcher m = p.matcher(s);
      return m.matches();
   }

   private boolean esValidoNumero(String texto) {
      int x;
      boolean esvalido = true;

      try {
         x = Integer.parseInt(texto);

      } catch (Exception e) {
         esvalido = false;
      }
      return esvalido;

   }

   public boolean verify(JComponent input) {

      if (input instanceof JTextField) {

         String texto = ((JTextField) input).getText();

         switch (variable) {
            case "Edad":
               if (!esValidoNumero(texto)) {
                  JOptionPane.showMessageDialog(input, "Error en Edad");
                  return false;
               }
               break;
            case "Email":
               if (!esValidoEmail(texto)) {
                  JOptionPane.showMessageDialog(input, "Error en Email");
                  return false;
               }
               break;
         }

      }
      return true;
   }
}
