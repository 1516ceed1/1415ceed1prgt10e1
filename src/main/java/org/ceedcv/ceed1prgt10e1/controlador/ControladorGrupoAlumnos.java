/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import static org.ceedcv.ceed1prgt10e1.controlador.ControladorGrupo.TITULOVENTANA;
import org.ceedcv.ceed1prgt10e1.modelo.Alumno;
import org.ceedcv.ceed1prgt10e1.modelo.Grupo;
import org.ceedcv.ceed1prgt10e1.modelo.IModelo;
import org.ceedcv.ceed1prgt10e1.vista.Funciones;
import org.ceedcv.ceed1prgt10e1.vista.VistaAlumno;
import org.ceedcv.ceed1prgt10e1.vista.VistaGrupo;
import org.ceedcv.ceed1prgt10e1.vista.VistaGrupoAlumnos;
import org.ceedcv.ceed1prgt10e1.vista.VistaPrincipal;

/**
 *
 * @author paco
 */
class ControladorGrupoAlumnos extends MouseAdapter implements ActionListener {

   private VistaGrupoAlumnos vistaGrupoAlumnos;
   private VistaPrincipal vistaPrincipal;

   private VistaGrupo vistaGrupo = null;
   private VistaAlumno vistaAlumno = null;

   private ControladorGrupo controladorGrupo = null;
   private ControladorAlumno controladorAlumno = null;

   private IModelo modelo;
   private Grupo registroActualGrupo;
   private Alumno registroActualAlumno;
   private String posicionRegistroGrupo;
   private String posicionRegistroAlumno;

   final static String TITULOVENTANA = "GRUPO ALUMNOS";
   final static String GRUPO = "GRUPO";
   final static String ALUMNO = "ALUMNO";
   final static String GASALIR = "GASALIR";

   final static String POSICIONFINAL = "Final";
   final static String POSICIONINICIAL = "Inicial";
   final static String POSICIONACTUAL = "Actual";

   private Grupo grupo;
   private ArrayList<Grupo> grupos;
   private ArrayList<Alumno> alumnos;

   private JTable tablaAlumnos;
   private JTable tablaGrupos;

   public ControladorGrupoAlumnos(VistaPrincipal vistaPrincipal, IModelo modelo) {

      this.vistaPrincipal = vistaPrincipal;
      this.modelo = modelo;
      inicializar();

   }

   private void inicializar() {

      Funciones f = new Funciones();
      f.cierraFrames(vistaPrincipal);

      vistaGrupoAlumnos = new VistaGrupoAlumnos();
      vistaGrupoAlumnos.setTitle(TITULOVENTANA);
      vistaPrincipal.getEscritorio().add(vistaGrupoAlumnos);

      inicializa();
      configurarVista();
      llenartablas();

   }

   @Override
   public void actionPerformed(ActionEvent e) {
      String comando = e.getActionCommand();
      switch (comando) {
         case GASALIR: // Exit
            salir();
            break;
         case GRUPO: // Exit
            grupo();
            break;
         case ALUMNO: // Exit
            alumno();
            break;
      }
   }

   @Override
   public void mouseClicked(MouseEvent e) {
      Object tabla = e.getSource();

      if (tabla == vistaGrupoAlumnos.getTablaGrupos()) {
         seleccionarRegistroGrupo(e);
      }

   }

   private void inicializa() {

      vistaGrupoAlumnos.getGrupo().setActionCommand(GRUPO);
      vistaGrupoAlumnos.getAlumno().setActionCommand(ALUMNO);
      vistaGrupoAlumnos.getjbSalir().setActionCommand(GASALIR);

      vistaGrupoAlumnos.getGrupo().addActionListener(this);
      vistaGrupoAlumnos.getAlumno().addActionListener(this);
      vistaGrupoAlumnos.getjbSalir().addActionListener(this);

      vistaGrupoAlumnos.getTablaGrupos().addMouseListener((MouseAdapter) this);
      //vistaGrupoAlumnos.getTablaAlumnos().addMouseListener((MouseAdapter) this);

   }

   private void salir() {
      vistaGrupoAlumnos.setVisible(false);
   }

   private void alumno() {
      controladorAlumno = new ControladorAlumno(vistaPrincipal, modelo);
   }

   private void grupo() {
      controladorGrupo = new ControladorGrupo(vistaPrincipal, modelo);
   }

   private void llenarTablaAlumnos(ArrayList<Alumno> lista, Grupo grupo) {

      Alumno alumno;
      DefaultTableModel tablaModelo;

      System.out.println("LLenando tabla alumnos");

      String[] titulos = {"Id", "Nombre", "Edad", "Email", "Fecha"};
      String[] reg = new String[titulos.length];
      tablaModelo = new DefaultTableModel(null, titulos);

      if (lista.isEmpty()) {

         tablaAlumnos.setVisible(false);

      } else {

         tablaAlumnos.setVisible(true);

         Iterator<Alumno> it = lista.iterator();

         while (it.hasNext()) {
            alumno = it.next();
            // Añadimos los alumnos del grupo
            if (alumno.getGrupo().getId() == grupo.getId()) {
               reg[0] = alumno.getId() + "";
               reg[1] = alumno.getNombre();
               reg[2] = alumno.getEdad() + "";
               reg[3] = alumno.getEmail();
               Date fecha = alumno.getFecha();
               Funciones f = new Funciones();
               reg[4] = f.ConvertirDatetoSTring(fecha);

               tablaModelo.addRow(reg);
            }
         }

         tablaAlumnos.setModel(tablaModelo);

      }
   }

   private void llenarTablaGrupos(ArrayList<Grupo> lista) {
      Grupo grupo;
      DefaultTableModel tablaModelo;

      if (!lista.isEmpty()) {
         tablaGrupos.setVisible(true);
         String[] titulos = {"Id", "Nombre"};

         String[] reg = new String[titulos.length];
         tablaModelo = new DefaultTableModel(null, titulos);

         Iterator<Grupo> it = lista.iterator();

         while (it.hasNext()) {
            grupo = it.next();
            reg[0] = grupo.getId() + "";
            reg[1] = grupo.getNombre();
            tablaModelo.addRow(reg);
         }
         tablaGrupos.setModel(tablaModelo);
      } else {
         tablaGrupos.setVisible(false);
      }
   }

   private void seleccionarRegistroGrupo(MouseEvent e) {
      // Ha cambiado el grupo seleccionado

      Grupo grupo = null;
      ArrayList<Alumno> alumnosg = new ArrayList();
      String id;
      int row;

      System.out.println("Seleccionado Registro Grupo");

      row = vistaGrupoAlumnos.getTablaGrupos().rowAtPoint(e.getPoint());

      if (row != -1) {
         id = (String) vistaGrupoAlumnos.getTablaGrupos().getValueAt(row, 0);
         grupo = getGrupo(id);
      }
      if (grupo != null) { // Hay grupos
         alumnosg = getAlumnosGrupo(grupo);
         llenarTablaAlumnos(alumnosg, grupo);
      }

   }

   private Grupo getGrupo(String id) {

      Grupo grupo = null;
      Integer id_ = Integer.parseInt(id);

      int talla = grupos.size();
      int i = 0;

      while (i < talla) {
         grupo = (Grupo) grupos.get(i);
         if (grupo.getId() == id_) {
            break;
         }
         i++;
      }
      return grupo;

   }

   public ArrayList<Alumno> getAlumnosGrupo(Grupo grupo) {

      ArrayList<Alumno> alumnosg = new ArrayList();
      int talla = alumnos.size();

      for (int i = 0; i < talla; i++) {
         if (alumnos.get(i).getGrupo().getId() == grupo.getId()) {
            alumnosg.add(alumnos.get(i));
         }
      }
      return alumnosg;

   }

   private void llenartablas() {

      grupos = modelo.readg();
      if (grupos != null) {
         tablaGrupos = vistaGrupoAlumnos.getTablaGrupos();
         llenarTablaGrupos(grupos);
         grupo = grupos.get(0);
         alumnos = modelo.reada();
         if (alumnos != null) {
            tablaAlumnos = vistaGrupoAlumnos.getTablaAlumnos();
            llenarTablaAlumnos(alumnos, grupo);
         }
      }
   }

   private void configurarVista() {
      vistaGrupoAlumnos.setTitle("GRUPO ALUMNOS");
      vistaGrupoAlumnos.setVisible(true);
      try {
         vistaGrupoAlumnos.setMaximum(true);
         vistaGrupoAlumnos.setSelected(true);
      } catch (java.beans.PropertyVetoException e) {
      }
   }

}
