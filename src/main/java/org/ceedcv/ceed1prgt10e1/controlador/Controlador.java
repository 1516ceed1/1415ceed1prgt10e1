package org.ceedcv.ceed1prgt10e1.controlador;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import org.ceedcv.ceed1prgt10e1.modelo.IModelo;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloDb4o;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloFichero;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloHibernate;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloMysql;
import org.ceedcv.ceed1prgt10e1.vista.VistaAcerca;
import org.ceedcv.ceed1prgt10e1.vista.VistaAlumno;
import org.ceedcv.ceed1prgt10e1.vista.VistaAlumnoTabla;
import org.ceedcv.ceed1prgt10e1.vista.VistaGrupo;
import org.ceedcv.ceed1prgt10e1.vista.VistaGrupoAlumnos;
import org.ceedcv.ceed1prgt10e1.vista.VistaGrupoTabla;
import org.ceedcv.ceed1prgt10e1.vista.VistaPrincipal;
import org.ceedcv.ceed1prgt10e1.vista.VistaPortada;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Controlador implements ActionListener {

    private IModelo modelo;
    private final String docuweb = "https://docs.google.com/document/d/1hZJuyaL9wOKDP2H4nLwAeGRXodnFlpBGOL24QbB-3rM/edit?usp=sharing";

    //VistaGrafica vista;
    private JInternalFrame jinternalFrame = null;
    private VistaPrincipal vistaPrincipal = null;
    private VistaGrupo vistaGrupo = null;
    private VistaAlumno vistaAlumno = null;
    private VistaGrupoAlumnos vistaGraficaGrupoAlumnos = null;
    private VistaAcerca vistaAcerca = null;
    private VistaPortada vistaPortada = null;

    private ControladorAlumno controladorAlumno = null;
    private ControladorGrupo controladorGrupo = null;
    private ControladorGrupoAlumnos controladorGrupoAlumnos;

    private String titulo1 = "Aplicación de Ejemplo. ";
    private String titulo;
    final static String ALUMNO = "ALUMNO";
    final static String ALUMNOTABLA = "ALUMNOTABLA";
    final static String GRUPO = "GRUPO";
    final static String GRUPOTABLA = "GRUPOTABLA";
    final static String GRUPOALUMNOS = "GRUPOALUMNOS";
    final static String MODELO = "MODELO";
    final static String WEB = "WEB";
    final static String ACERCA = "ACERCA";
    final static String PDF = "PDF";
    final static String INSTALAR = "INSTALAR";
    final static String DESINSTALAR = "DEINSTALAR";
    final static String PORTADA = "PORTADA";
    final static String SALIR = "SALIR";

    Controlador(IModelo modelo_, VistaPrincipal vista_) throws IOException {
        vistaPrincipal = vista_;
        modelo = modelo_;

        seleccionaModelo();
        inicializa();
    }

    private void seleccionaModelo() {

        try {
            String[] modelos = {"Fichero", "MySQL", "Db4o", "Hibernate"};

            String selec = (String) JOptionPane.showInputDialog(null, "Selecciona el modelo", "Modelo",
              JOptionPane.QUESTION_MESSAGE, null, modelos, modelos[2]);

            titulo = titulo1;
            vistaPrincipal.getMenuHibernate().setEnabled(false);
            switch (selec) {
                case "Fichero":
                    modelo = new ModeloFichero();
                    titulo = titulo + "Tema 6. Ficheros";
                    break;
                case "MySQL":
                    modelo = new ModeloMysql();
                    titulo = titulo + "Tema 8. Mysql";
                    break;
                case "Db4o":
                    modelo = new ModeloDb4o();
                    titulo = titulo + "Tema 9. Db4o";
                    break;
                case "Hibernate":
                    modelo = new ModeloHibernate();
                    vistaPrincipal.getMenuHibernate().setEnabled(true);
                    titulo = titulo + "Tema 10. Hibernate";
                    break;
                default:
                    salir();
                    break;

            }

            vistaPrincipal.setTitle(titulo);
            vistaPrincipal.getTitulo().setText(titulo);
            portada();

        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch (comando) {
            case SALIR:
                salir();
                break;
            case ALUMNO:
                alumno();
                break;
            case ALUMNOTABLA:
                alumnotabla();
                break;
            case GRUPO:
                grupo();
                break;
            case GRUPOTABLA:
                grupotabla();
                break;
            case GRUPOALUMNOS:
                grupoalumnos();
                break;
            case MODELO:
                seleccionaModelo();
                break;
            case WEB:
                web();
                break;
            case PDF:
                pdf();
                break;
            case INSTALAR:
                instalar();
                break;
            case DESINSTALAR:
                desinstalar();
                break;
            case ACERCA:
                acerca();
                break;
            case PORTADA:
                portada();
                break;
        }

    }

    private void salir() {
        vistaPrincipal.setVisible(false);
        System.exit(0);
    }

    private void inicializa() {

        vistaPrincipal.getAlumno().setActionCommand(ALUMNO);
        vistaPrincipal.getAlumnosTabla().setActionCommand(ALUMNOTABLA);
        vistaPrincipal.getGrupo().setActionCommand(GRUPO);
        vistaPrincipal.getGrupoTabla().setActionCommand(GRUPOTABLA);
        vistaPrincipal.getGrupoAlumnos().setActionCommand(GRUPOALUMNOS);
        vistaPrincipal.getWeb().setActionCommand(WEB);
        vistaPrincipal.getAcerca().setActionCommand(ACERCA);

        vistaPrincipal.getInstalar().setActionCommand(INSTALAR);
        vistaPrincipal.getDesinstalar().setActionCommand(DESINSTALAR);
        vistaPrincipal.getPdf().setActionCommand(PDF);
        vistaPrincipal.getSalir().setActionCommand(SALIR);
        vistaPrincipal.getModelo().setActionCommand(MODELO);

        vistaPrincipal.getAlumno().addActionListener(this);
        vistaPrincipal.getAlumnosTabla().addActionListener(this);
        vistaPrincipal.getGrupo().addActionListener(this);
        vistaPrincipal.getGrupoTabla().addActionListener(this);
        vistaPrincipal.getGrupoAlumnos().addActionListener(this);
        vistaPrincipal.getWeb().addActionListener(this);
        vistaPrincipal.getAcerca().addActionListener(this);

        vistaPrincipal.getInstalar().addActionListener(this);
        vistaPrincipal.getDesinstalar().addActionListener(this);
        vistaPrincipal.getPdf().addActionListener(this);
        vistaPrincipal.getSalir().addActionListener(this);
        vistaPrincipal.getModelo().addActionListener(this);

        vistaPrincipal.setLocationRelativeTo(null); // Centrar
        vistaPrincipal.setVisible(true);

    }

    private void portada() {

        //vistaPortada = vistaPortada.getInstancia();
        vistaPortada = new VistaPortada();
        vistaPrincipal.getEscritorio().add(vistaPortada);
        vistaPortada.getTitulo().setText(titulo);
        vistaPortada.setTitle(PORTADA);
        vistaPortada.setVisible(true);
        try {
            vistaPortada.setMaximum(true);
            vistaPortada.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {
        }

    }

    private void acerca() {

        VistaAcerca vistaAcerca = new VistaAcerca();
        vistaPrincipal.getEscritorio().add(vistaAcerca);
        vistaAcerca.setTitle(ACERCA);
        vistaAcerca.getTitulo().setText(titulo);

        vistaAcerca.setVisible(true);
        try {
            vistaAcerca.setMaximum(true);
            vistaAcerca.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {
        }

    }

    private void alumno() {

        controladorAlumno = new ControladorAlumno(vistaPrincipal, modelo);
    }

    private void grupo() {

        controladorGrupo = new ControladorGrupo(vistaPrincipal, modelo);

    }

    private void grupoalumnos() {

        controladorGrupoAlumnos = new ControladorGrupoAlumnos(vistaPrincipal, modelo);
    }

    private void pdf() {
        try {
            String filename = "src/main/java/org/ceedcv/ceed1prgt10e1/documentacion/docu.pdf";
            File doc = new File(filename);
            Desktop.getDesktop().open(doc);
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void web() {

        Desktop desktop = Desktop.getDesktop();

        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(docuweb));
            } catch (URISyntaxException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            pdf();
        }
    }

    private void instalar() {
        modelo.instalar();
    }

    private void desinstalar() {
        modelo.desinstalar();
        if (controladorGrupo != null) {
            controladorGrupo.inicializaGrupos();
            controladorGrupo.visible(false);
        }
        //controladorAlumno.inicializaAlumno();

    }

    private void alumnotabla() {

        JInternalFrame jiframe = new JInternalFrame("AlumnoTabla");
        VistaAlumnoTabla jpanel = new VistaAlumnoTabla();
        jiframe.add(jpanel);
        vistaPrincipal.getEscritorio().add(jiframe);

        jiframe.setVisible(true);
        try {
            jiframe.setMaximum(true);
            jiframe.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {
        }
    }

    private void grupotabla() {

        JInternalFrame jiframe = new JInternalFrame("GrupoTabla");
        VistaGrupoTabla jpanel = new VistaGrupoTabla();
        jiframe.add(jpanel);
        vistaPrincipal.getEscritorio().add(jiframe);

        jiframe.setVisible(true);
        try {
            jiframe.setMaximum(true);
            jiframe.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {
        }
    }

}
