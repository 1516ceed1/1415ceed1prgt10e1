package org.ceedcv.ceed1prgt10e1.controlador;

import java.io.IOException;
import org.ceedcv.ceed1prgt10e1.modelo.IModelo;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloDb4o;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloHibernate;
import org.ceedcv.ceed1prgt10e1.modelo.ModeloMysql;
import org.ceedcv.ceed1prgt10e1.vista.VistaPrincipal;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 22-feb-2016
 */
public class Main {

    public static void main(String[] args) throws IOException {

        //IModelo modelo = new ModeloFichero();
        //IModelo modelo = new ModeloMysql();
        //IModelo modelo = new ModeloDb4o();
        IModelo modelo = null;
        VistaPrincipal vista = new VistaPrincipal();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
