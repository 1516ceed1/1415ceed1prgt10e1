/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JFrame;

import org.ceedcv.ceed1prgt10e1.modelo.Alumno;
import org.ceedcv.ceed1prgt10e1.modelo.Grupo;

import org.ceedcv.ceed1prgt10e1.modelo.IModelo;
import org.ceedcv.ceed1prgt10e1.vista.Funciones;
import org.ceedcv.ceed1prgt10e1.vista.Verificador;
import org.ceedcv.ceed1prgt10e1.vista.VistaAlumno;
import org.ceedcv.ceed1prgt10e1.vista.VistaPrincipal;

/**
 * @author paco
 */
public class ControladorAlumno implements ActionListener {

    VistaAlumno vistaAlumno = null;
    VistaPrincipal vistaPrincipal = null;
    final static String TITULOVENTANA = "ALUMNO";

    IModelo modelo;
    Alumno actual;
    int iactual;
    ArrayList alumnos = new ArrayList();
    String operacion = ""; // Se guarda la ultima operacion realizada

    // Operaciones: CRUD
    final static String ACREATE = "ACREATE";
    final static String AREAD = "AREAD";
    final static String AUPDATE = "AUPDATE";
    final static String ADELETE = "ADELETE";
    final static String ASALIR = "ASALIR";

    final static String APRIMERO = "APRIMERO";
    final static String AULTIMO = "AULTIMO";
    final static String ASIGUIENTE = "ASIGUIENTE";
    final static String AANTERIOR = "AANTERIOR";

    final static String AGUARDAR = "AGUARDAR";
    final static String ACANCELAR = "ACANCELAR";

    ControladorAlumno(VistaPrincipal vistaPrincipal, IModelo modelo) {

        this.vistaPrincipal = vistaPrincipal;
        this.modelo = modelo;
        inicializa();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Alumno alumno;

        switch (comando) {

            case APRIMERO:
                primero();
                mostrar(actual);
                break;

            case AULTIMO:
                ultimo();
                mostrar(actual);
                break;

            case ASIGUIENTE:
                siguiente();
                mostrar(actual);
                break;

            case AANTERIOR:
                anterior();
                mostrar(actual);
                break;

            case ASALIR: // Exit
                salir();
                break;

            case ACREATE: // Create
                operacion = ACREATE;
                editarCampos(true);
                activaCRUD(false);
                rellenarCombobox(null);
                vistaAlumno.getjtfId().setText("");
                vistaAlumno.getjtfId().setEnabled(false);
                verificar();
                break;

            case AGUARDAR:
                // Analiza la ultima operacion realizada
                switch (operacion) {
                    case ACREATE:
                        alumno = obtener();
                        modelo.create(alumno);
                        alumnos.add(alumno);
                        actual = alumno;
                        iactual = alumnos.size() - 1; // Añade por el final
                        break;
                    case ADELETE:
                        alumno = obtener();
                        modelo.delete(alumno);
                        alumnos.remove(iactual);
                        anterior();
                        break;
                    case AUPDATE:  // Actualizar
                        alumno = obtener();
                        modelo.update(alumno);
                        actual = alumno;
                        alumnos = modelo.reada();
                        break;
                    case AREAD:
                        alumno = obtenerId();
                        int id = alumno.getId();
                        getAlumno(id);
                        break;
                }
                mostrar(actual);
                operacion = "";
                break;
            // Fin Guardar

            case ACANCELAR:
                mostrar(actual);
                operacion = "";
                break;

            case AREAD: // Read
                operacion = AREAD;
                vistaAlumno.getjtfId().setText("");
                vaciarCampos();
                editarCampos(false);
                activaCRUD(false);
                vistaAlumno.getjtfId().setEditable(true); // Pedimos solo la id.

                break;

            case AUPDATE:  // Actualizar
                operacion = AUPDATE;
                editarCampos(true);
                activaCRUD(false);
                verificar();
                break;

            case ADELETE: // Borrar
                operacion = ADELETE;
                activaCRUD(false);
                break;

        }
    }

    public void vaciarCampos() {
        vistaAlumno.getjtfId().setText("");
        vistaAlumno.getjtfNombre().setText("");
        vistaAlumno.getjtfEdad().setText("");
        vistaAlumno.getjtfEmail().setText("");
        vistaAlumno.getjcbGrupo().addItem("");
    }

    public void editarCampos(Boolean b) {
        vistaAlumno.getjtfId().setEditable(b);
        vistaAlumno.getjtfNombre().setEditable(b);
        vistaAlumno.getjtfEmail().setEditable(b);
        vistaAlumno.getjtfEdad().setEditable(b);
        vistaAlumno.getjcbGrupo().setEnabled(b);
        vistaAlumno.getjdcFecha().setEnabled(b);
    }

    public void activaCRUD(Boolean b) {
        vistaAlumno.getjbGuardar().setEnabled(!b);
        vistaAlumno.getjbCancelar().setEnabled(!b);
        vistaAlumno.getjbSalir().setEnabled(b);
        vistaAlumno.getjbCreate().setEnabled(b);
        vistaAlumno.getjbRead().setEnabled(b);
        vistaAlumno.getjbUpdate().setEnabled(b);
        vistaAlumno.getjbDelete().setEnabled(b);
    }

    private void salir() {
        vistaAlumno.setVisible(false);
    }

    private void inicializa() {

        Funciones f = new Funciones();
        f.cierraFrames(vistaPrincipal);

        vistaAlumno = new VistaAlumno();
        vistaAlumno.setTitle(TITULOVENTANA);
        vistaPrincipal.getEscritorio().add(vistaAlumno);

        vistaAlumno.getjbPrimero().setActionCommand(APRIMERO);
        vistaAlumno.getjbUltimo().setActionCommand(AULTIMO);
        vistaAlumno.getjbSiguiente().setActionCommand(ASIGUIENTE);
        vistaAlumno.getjbAnterior().setActionCommand(AANTERIOR);
        vistaAlumno.getjbGuardar().setActionCommand(AGUARDAR);
        vistaAlumno.getjbCancelar().setActionCommand(ACANCELAR);
        vistaAlumno.getjbCreate().setActionCommand(ACREATE);
        vistaAlumno.getjbRead().setActionCommand(AREAD);
        vistaAlumno.getjbUpdate().setActionCommand(AUPDATE);
        vistaAlumno.getjbDelete().setActionCommand(ADELETE);
        vistaAlumno.getjbSalir().setActionCommand(ASALIR);

        vistaAlumno.getjbPrimero().addActionListener(this);
        vistaAlumno.getjbUltimo().addActionListener(this);
        vistaAlumno.getjbSiguiente().addActionListener(this);
        vistaAlumno.getjbAnterior().addActionListener(this);

        vistaAlumno.getjbGuardar().addActionListener(this);
        vistaAlumno.getjbCancelar().addActionListener(this);

        vistaAlumno.getjbCreate().addActionListener(this);
        vistaAlumno.getjbRead().addActionListener(this);
        vistaAlumno.getjbUpdate().addActionListener(this);
        vistaAlumno.getjbDelete().addActionListener(this);
        vistaAlumno.getjbSalir().addActionListener(this);

        vistaAlumno.setTitle("ALUMNO");
        //vistaAlumno.setLocationRelativeTo(null); // Centrar JFrame

        try {
            vistaAlumno.setMaximum(true);
            vistaAlumno.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {
        }
        vistaAlumno.setVisible(true);

        alumnos = modelo.reada();
        if (alumnos.size() > 0) {
            primero();
        }
        mostrar(actual);

    }

    private void mostrar(Alumno primero) {

        editarCampos(false);
        activaCRUD(true);

        if (primero == null) {
            return;
        }

        rellenarCombobox(primero);

        if (primero == null) {
            return;
        }

        vistaAlumno.getjtfId().setText(primero.getId() + "");
        vistaAlumno.getjtfNombre().setText(primero.getNombre());
        vistaAlumno.getjtfEdad().setText(primero.getEdad() + "");
        vistaAlumno.getjtfEmail().setText(primero.getEmail());
        vistaAlumno.getjdcFecha().setDate(primero.getFecha());

    }

    private Alumno obtenerId() {
        Alumno alumno = new Alumno();
        alumno.setId(Integer.parseInt(vistaAlumno.getjtfId().getText()));
        return alumno;
    }

    private Alumno obtener() {
        Alumno alumno = new Alumno();

        // La validación se hace con validate
        String id_ = vistaAlumno.getjtfId().getText();
        if (id_.length() == 0) {
            id_ = "0";
        }
        int id = Integer.parseInt(id_);
        String edad_ = vistaAlumno.getjtfEdad().getText();
        int edad = Integer.parseInt(edad_);
        String nombre = vistaAlumno.getjtfNombre().getText();
        String email = vistaAlumno.getjtfEmail().getText();
        Date fecha = vistaAlumno.getjdcFecha().getDate();

        alumno.setId(id);
        alumno.setNombre(nombre);
        alumno.setEdad(edad);
        alumno.setEmail(email);
        alumno.setFecha(fecha);

        Object[] sgrupo = null;
        //if (vistaAlumno.getjcbGrupo().getSelectedObjects().length > 0) {
        sgrupo = vistaAlumno.getjcbGrupo().getSelectedObjects();
        Grupo grupo = (Grupo) sgrupo[0];
        alumno.setGrupo(grupo);
        //}

        return alumno;

    }

    private void getAlumno(int id) {

        Boolean encontrado = false;
        Alumno alumno = null;

        int talla = alumnos.size();
        int i = 0;

        while (i < talla && encontrado == false) {
            alumno = (Alumno) alumnos.get(i);
            if (alumno.getId() == id) {
                encontrado = true;
                iactual = i;
            }
            i++;
        }

        if (encontrado == true) {
            actual = alumno;
        } else {
            iactual = 0;
            actual = (Alumno) alumnos.get(iactual);
        }
    }

    private void anterior() {
        if (iactual != 0) {
            iactual--;
            actual = (Alumno) alumnos.get(iactual);
        }
    }

    private void siguiente() {
        if (iactual != alumnos.size() - 1) {
            iactual++;
            actual = (Alumno) alumnos.get(iactual);
        }
    }

    private void ultimo() {
        iactual = alumnos.size() - 1;
        actual = (Alumno) alumnos.get(iactual);
    }

    private void primero() {

        if (alumnos != null) {
            actual = (Alumno) alumnos.get(iactual);
            iactual = 0;
        } else {
            actual = null;
            iactual = -1;
        }
    }

    private void verificar() {
        vistaAlumno.getjtfEdad().setInputVerifier(new Verificador("Edad"));
        vistaAlumno.getjtfEmail().setInputVerifier(new Verificador("Email"));
    }

    private void rellenarCombobox(Alumno alumno) {

        ArrayList grupos = modelo.readg();
        if (grupos != null) {
            vistaAlumno.getjcbGrupo().removeAllItems();
            vistaAlumno.getjcbGrupo().addItem("No existe");

            for (int i = 0; i < grupos.size(); i++) {
                vistaAlumno.getjcbGrupo().addItem(grupos.get(i));

                Grupo grupo = (Grupo) grupos.get(i);

                if (alumno != null && alumno.getGrupo() != null) {
                    if (grupo.getId() == alumno.getGrupo().getId()) { // Coincide
                        vistaAlumno.getjcbGrupo().setSelectedItem(grupos.get(i));

                    }
                }
            }
        }
    }

    void inicializaAlumno() {
        alumnos.clear();
    }

}
