/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed1prgt10e1.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import org.ceedcv.ceed1prgt10e1.modelo.Grupo;

import org.ceedcv.ceed1prgt10e1.modelo.IModelo;
import org.ceedcv.ceed1prgt10e1.vista.Funciones;
import org.ceedcv.ceed1prgt10e1.vista.VistaGrupo;
import org.ceedcv.ceed1prgt10e1.vista.VistaPrincipal;

/**
 *
 * @author paco
 */
public class ControladorGrupo implements ActionListener {

    private IModelo modelo;
    private VistaGrupo vistaGrupo;
    private VistaPrincipal vistaPrincipal;
    private ArrayList grupos;
    private Grupo actual;
    private int iactual;
    private String operacion;

    final static String TITULOVENTANA = "GRUPO";
    final static String GCREATE = "GCREATE";
    final static String GREAD = "GREAD";
    final static String GUPDATE = "GUPDATE";
    final static String GDELETE = "GDELETE";
    final static String GSALIR = "GSALIR";

    final static String GPRIMERO = "GPRIMERO";
    final static String GULTIMO = "GULTIMO";
    final static String GSIGUIENTE = "GSIGUIENTE";
    final static String GANTERIOR = "GANTERIOR";
    final static String GGUARDAR = "GGUARDAR";
    final static String GCANCELAR = "GCANCELAR";

    ControladorGrupo(VistaPrincipal vistaPrincipal, IModelo modelo) {
        this.vistaPrincipal = vistaPrincipal;
        this.modelo = modelo;
        inicializa();
    }

    private void inicializa() {

        Funciones f = new Funciones();
        f.cierraFrames(vistaPrincipal);

        vistaGrupo = new VistaGrupo();
        vistaGrupo.setTitle(TITULOVENTANA);
        vistaPrincipal.getEscritorio().add(vistaGrupo);

        inicializaNavegables();
        inicializaCrud();

        visible(true);
        try {
            vistaGrupo.setMaximum(true);
            vistaGrupo.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {
        }
        grupos = modelo.readg();
        if (grupos.size() > 0) {
            primero();
        }
        mostrar(actual);

    }

    public void visible(Boolean valor) {
        vistaGrupo.setVisible(valor);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();
        switch (comando) {
            case GPRIMERO:
                primero();
                mostrar(actual);
                break;
            case GULTIMO:
                ultimo();
                mostrar(actual);
                break;
            case GSIGUIENTE:
                siguiente();
                mostrar(actual);
                break;
            case GANTERIOR:
                anterior();
                mostrar(actual);
                break;
            case GCREATE: // Create
                operacion = GCREATE;
                vistaGrupo.getjtfId().setText("");
                vistaGrupo.getjtfNombre().setText("");

                vistaGrupo.getjtfId().setEditable(false);
                vistaGrupo.getjtfId().setEnabled(false);
                vistaGrupo.getjtfNombre().setEditable(true);

                activaCRUD(false);
                break;

            case GGUARDAR:
                guardar(operacion);
                mostrar(actual);
                operacion = "";
                break;

            case GCANCELAR:
                mostrar(actual);
                operacion = "";
                break;

            case GREAD: // Read
                operacion = GREAD;
                vistaGrupo.getjtfId().setText("");
                vistaGrupo.getjtfNombre().setText("");

                vistaGrupo.getjtfId().setEditable(true);
                vistaGrupo.getjtfNombre().setEditable(false);

                activaCRUD(false);
                vistaGrupo.getjtfId().setFocusable(true);

                break;

            case GUPDATE:  // Actualizar
                operacion = GUPDATE;
                vistaGrupo.getjtfId().setEditable(true);
                vistaGrupo.getjtfNombre().setEditable(true);
                activaCRUD(false);

                break;

            case GDELETE: // Borrar
                operacion = GDELETE;
                activaCRUD(false);
                break;

            case GSALIR: // Exit
                visible(false);
                break;
        }

    }

    private void mostrar(Grupo primero) {

        editarCampos(false);
        activaCRUD(true);

        if (primero == null) {
            return;
        }

        vistaGrupo.getjtfId().setText("" + primero.getId());
        vistaGrupo.getjtfNombre().setText(primero.getNombre());

    }

    public void activaCRUD(Boolean b) {
        vistaGrupo.getjbGuardar().setEnabled(!b);
        vistaGrupo.getjbCancelar().setEnabled(!b);
        vistaGrupo.getjbSalir().setEnabled(b);
        vistaGrupo.getjbRead().setEnabled(b);
        vistaGrupo.getjbUpdate().setEnabled(b);
        vistaGrupo.getjbDelete().setEnabled(b);
        vistaGrupo.getjbCreate().setEnabled(b);
    }

    private Grupo obtener() {
        Grupo grupo = new Grupo();
        Integer id_;

        String id = vistaGrupo.getjtfId().getText();
        if (id.length() > 0) {
            id_ = Integer.parseInt(id);
        } else {
            id_ = 0;
        }

        grupo.setId(id_);
        String nombre = vistaGrupo.getjtfNombre().getText();

        grupo.setNombre(nombre);

        return grupo;
    }

    private void getGrupo(String id) {

        Boolean encontrado = false;
        Grupo grupo = null;

        int talla = grupos.size();
        int i = 0;

        while (i < talla && encontrado == false) {
            grupo = (Grupo) grupos.get(i);
            String grupoid = grupo.getId() + "";
            if (grupoid.equals(id)) {
                encontrado = true;
                iactual = i;
            }
            i++;
        }

        if (encontrado == true) {
            actual = grupo;
        } else {
            iactual = 0;
            actual = (Grupo) grupos.get(iactual);
        }
    }

    private void anterior() {
        if (iactual != 0) {
            iactual--;
            actual = (Grupo) grupos.get(iactual);
        }
    }

    private void siguiente() {
        if (iactual != grupos.size() - 1) {
            iactual++;
            actual = (Grupo) grupos.get(iactual);
        }
    }

    private void ultimo() {
        iactual = grupos.size() - 1;
        actual = (Grupo) grupos.get(iactual);
    }

    private void primero() {

        if (grupos != null) {
            iactual = 0;
            actual = (Grupo) grupos.get(iactual);

        } else {
            actual = null;
            iactual = -1;
        }

    }

    private void inicializaNavegables() {
        vistaGrupo.getjbPrimero().setActionCommand(GPRIMERO);
        vistaGrupo.getjbUltimo().setActionCommand(GULTIMO);
        vistaGrupo.getjbSiguiente().setActionCommand(GSIGUIENTE);
        vistaGrupo.getjbAnterior().setActionCommand(GANTERIOR);
        vistaGrupo.getjbPrimero().addActionListener(this);
        vistaGrupo.getjbUltimo().addActionListener(this);
        vistaGrupo.getjbSiguiente().addActionListener(this);
        vistaGrupo.getjbAnterior().addActionListener(this);
    }

    private void inicializaCrud() {
        vistaGrupo.getjbCreate().setActionCommand(GCREATE);
        vistaGrupo.getjbRead().setActionCommand(GREAD);
        vistaGrupo.getjbUpdate().setActionCommand(GUPDATE);
        vistaGrupo.getjbDelete().setActionCommand(GDELETE);
        vistaGrupo.getjbSalir().setActionCommand(GSALIR);
        vistaGrupo.getjbCreate().addActionListener(this);
        vistaGrupo.getjbRead().addActionListener(this);
        vistaGrupo.getjbUpdate().addActionListener(this);
        vistaGrupo.getjbDelete().addActionListener(this);
        vistaGrupo.getjbSalir().addActionListener(this);

        vistaGrupo.getjbGuardar().setActionCommand(GGUARDAR);
        vistaGrupo.getjbCancelar().setActionCommand(GCANCELAR);
        vistaGrupo.getjbGuardar().addActionListener(this);
        vistaGrupo.getjbCancelar().addActionListener(this);
    }

    private void guardar(String operacion) {

        Grupo grupo = new Grupo();
        switch (operacion) {
            case GCREATE:
                grupo = obtener();
                modelo.create(grupo);
                grupos.add(grupo);
                actual = grupo;
                iactual = grupos.size() - 1;
                break;
            case GDELETE:
                grupo = obtener();
                modelo.delete(grupo);
                grupos = modelo.readg();
                anterior();
                break;
            case GUPDATE:  // Actualizar
                grupo = obtener();
                modelo.update(grupo);
                actual = grupo;
                grupos = modelo.readg();
                break;
            case GREAD:
                grupo = obtener();
                String id = grupo.getId() + "";
                getGrupo(id);
                break;
        }
    }

    private void editarCampos(boolean b) {
        vistaGrupo.getjtfId().setEditable(b);
        vistaGrupo.getjtfNombre().setEditable(b);
    }

    public void inicializaGrupos() {
        grupos.clear();
    }

}
